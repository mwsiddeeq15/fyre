Requires:
Nodejs(and NPM)

This repo is ***For Your Reusable/React Elements*** (This is a ***FYRE*** name if I do say so myself, but if no one shares the excitement I can take naming suggestions now!)

-The purpose of this is to house our library of reusable React components while using react-styleguidist to demo their functionality and implicitly maintain proper documentation on their usage. Our own little documentation. 

-TODO: Set up typescript and create npm package (can set a hook to auto pack and deploy on commit??).
 
# FYRE Development
From /fyre

$ npm i

$ npm start